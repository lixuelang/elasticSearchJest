package com.xuelang.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xuelang.es.base.dto.PageDto;
import com.xuelang.es.base.dto.PageModel;
import com.xuelang.es.model.Car;
import com.xuelang.es.service.CarEsService;

@RestController
@RequestMapping("car")
public class CarController {
    private static final Logger LOG = LoggerFactory.getLogger(CarController.class);

    @Autowired
    private CarEsService carEsService;

    @RequestMapping("add")
    public Boolean add() {
        Car car = new Car();
        car.setId(1L);
        car.setPrice(new BigDecimal(10000));
        car.setColor("red");
        car.setMake("honda");
        car.setSold(1414464026000L);

        return carEsService.add(car);
    }

    @RequestMapping("addList")
    public Boolean addList() {
        List<Car> list = createList();

        carEsService.addList(list);
        LOG.info("添加成功");
        return true;
    }

    @RequestMapping("update")
    public Car update() {
        Car car = new Car();
        // car.setId(1L);
        /*car.setPrice(new BigDecimal(10000));
        car.setColor("red");
        car.setMake("honda");*/
        car.setSold(1392172826000L);
        carEsService.update(8, car);
        return carEsService.getById(8);

    }

    @RequestMapping("getById")
    public Car getById(Long id) {
        return carEsService.getById(id);
    }

    @RequestMapping("list")
    public List<Car> list(Car car) {
        return carEsService.list(car);
    }

    @RequestMapping("page")
    public PageDto<Car> page(Car car, PageModel pageModel) {
        return carEsService.page(car, pageModel);
    }

    // 按颜色聚合
    @RequestMapping("aggs1")
    public Object aggs1() {
        return carEsService.aggs1();
    }

    // 按颜色聚合 + 均价
    @RequestMapping("aggs2")
    public Object aggs2() {
        return carEsService.aggs2();
    }

    // 按颜色聚合 + 均价 + 生产商
    @RequestMapping("aggs3")
    public Object aggs3() {
        return carEsService.aggs3();
    }

    // 每个汽车生成商计算最低和最高的价格
    @RequestMapping("aggs4")
    public Object aggs4() {
        return carEsService.aggs4();
    }

    // 条形图
    @RequestMapping("aggs5")
    public Object aggs5() {
        return carEsService.aggs5();
    }

    // 条形图
    @RequestMapping("aggs6")
    public Object aggs6() {
        return carEsService.aggs6();
    }

    // 按时间统计
    @RequestMapping("aggs7")
    public Object aggs7() {
        return carEsService.aggs7();
    }

    // 按季度展示所有汽车品牌总销售额。同时按季度、按每个汽车品牌计算销售总额
    @RequestMapping("aggs8")
    public Object aggs8() {
        return carEsService.aggs8();
    }

    // 全局桶
    @RequestMapping("aggs9")
    public Object aggs9() {
        return carEsService.aggs9();
    }

    // 过滤
    @RequestMapping("aggs10")
    public Object aggs10() {
        return carEsService.aggs10();
    }

    // 后过滤器
    @RequestMapping("aggs11")
    public Object aggs11() {
        return carEsService.aggs11();
    }

    // 桶排序
    @RequestMapping("aggs12")
    public Object aggs12() {
        return carEsService.aggs12();
    }

    // 桶排序 » 按度量排序
    @RequestMapping("aggs13")
    public Object aggs13() {
        return carEsService.aggs13();
    }

    // 多桶排序 » 基于“深度”度量排序
    @RequestMapping("aggs14")
    public Object aggs14() {
        return carEsService.aggs14();
    }

    // 统计去重后的数量
    @RequestMapping("aggs15")
    public Object aggs15() {
        return carEsService.aggs15();
    }

    private List<Car> createList() {
        List<Car> list = new ArrayList<>();
        Car car1 = new Car();
        car1.setId(2L);
        car1.setPrice(new BigDecimal(20000));
        car1.setColor("red");
        car1.setMake("honda");
        car1.setSold(1415155226000L);

        list.add(car1);

        Car car2 = new Car();
        car2.setId(3L);
        car2.setPrice(new BigDecimal(30000));
        car2.setColor("green");
        car2.setMake("ford");
        car2.setSold(1400380826000L);

        list.add(car2);

        Car car3 = new Car();
        car3.setId(4L);
        car3.setPrice(new BigDecimal(15000));
        car3.setColor("blue");
        car3.setMake("toyota");
        car3.setSold(System.currentTimeMillis());

        list.add(car3);

        Car car4 = new Car();
        car4.setId(5L);
        car4.setPrice(new BigDecimal(12000));
        car4.setColor("green");
        car4.setMake("toyota");
        car4.setSold(1404268826000L);

        list.add(car4);

        Car car5 = new Car();
        car5.setId(6L);
        car5.setPrice(new BigDecimal(20000));
        car5.setColor("red");
        car5.setMake("honda");
        car5.setSold(1415155226000L);

        list.add(car5);

        Car car6 = new Car();
        car6.setId(7L);
        car6.setPrice(new BigDecimal(80000));
        car6.setColor("red");
        car6.setMake("bmw");
        car6.setSold(1388544026000L);

        list.add(car6);

        Car car7 = new Car();
        car7.setId(8L);
        car7.setPrice(new BigDecimal(25000));
        car7.setColor("blue");
        car7.setMake("ford");
        car7.setSold(1392172826000L);

        list.add(car7);

        return list;
    }
}
