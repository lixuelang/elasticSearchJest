package com.xuelang.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xuelang.es.model.Website;
import com.xuelang.es.service.WebsiteEsService;

import io.searchbox.core.BulkResult;

@RestController
@RequestMapping("website")
public class WebsiteController {
    private static final Logger LOG = LoggerFactory.getLogger(WebsiteController.class);

    @Autowired
    private WebsiteEsService websiteEsSercice;

    @RequestMapping("addList")
    public BulkResult addList() {
        List<Website> list = createList();
        return websiteEsSercice.addList(list);
    }

    @RequestMapping("agg1")
    public Object agg1() {
        return websiteEsSercice.agg1();
    }

    private List<Website> createList() {
        List<Website> list = new ArrayList<>();
        list.add(new Website(1L, 100L, "US", dateToStamp("2014-10-28")));
        list.add(new Website(2L, 80L, "US", dateToStamp("2014-10-29")));
        list.add(new Website(3L, 99L, "US", dateToStamp("2014-10-29")));
        list.add(new Website(4L, 102L, "US", dateToStamp("2014-10-28")));
        list.add(new Website(5L, 75L, "US", dateToStamp("2014-10-28")));
        list.add(new Website(6L, 82L, "US", dateToStamp("2014-10-29")));
        list.add(new Website(7L, 100L, "EU", dateToStamp("2014-10-28")));
        list.add(new Website(8L, 280L, "EU", dateToStamp("2014-10-29")));
        list.add(new Website(9L, 155L, "EU", dateToStamp("2014-10-29")));
        list.add(new Website(10L, 623L, "EU", dateToStamp("2014-10-28")));
        list.add(new Website(11L, 380L, "EU", dateToStamp("2014-10-28")));
        list.add(new Website(12L, 319L, "EU", dateToStamp("2014-10-29")));

        return list;

    }

    /*
     * 将时间转换为时间戳
     */
    private long dateToStamp(String s) {
        // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd
        // HH:mm:ss");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

}
