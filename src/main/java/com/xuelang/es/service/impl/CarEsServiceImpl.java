package com.xuelang.es.service.impl;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.ConstantScoreQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.AggregatorFactories;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.ExtendedBounds;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xuelang.es.base.dto.PageDto;
import com.xuelang.es.base.dto.PageModel;
import com.xuelang.es.base.service.BaseEsService;
import com.xuelang.es.model.Car;
import com.xuelang.es.service.CarEsService;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Search;

@Service
public class CarEsServiceImpl extends BaseEsService<Car> implements CarEsService {
    private static final Logger LOG = LoggerFactory.getLogger(CarEsServiceImpl.class);

    private static final String INDEX = "car";
    private static final String TYPE = "index";

    @Autowired
    private JestClient jestClient;

    // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_aggregation_test_drive.html
    // http://shihlei.iteye.com/blog/2411470
    public CarEsServiceImpl() {
        super(INDEX, TYPE);
    }

    @Override
    public List<Car> list(Car query) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        if (null != query) {
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

            if (StringUtils.isNotBlank(query.getMake())) {
                boolQueryBuilder.must(QueryBuilders.termQuery("make", query.getMake()));
            }
            if (StringUtils.isNotBlank(query.getColor())) {
                boolQueryBuilder.must(QueryBuilders.termQuery("color", query.getColor()));
            }

            searchSourceBuilder.query(boolQueryBuilder);
        }
        // 排序
        searchSourceBuilder.sort("id", SortOrder.ASC);

        return super.search(searchSourceBuilder);
    }

    @Override
    public PageDto<Car> page(Car query, PageModel pageModel) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        if (null != query) {
            BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

            if (StringUtils.isNotBlank(query.getMake())) {
                boolQueryBuilder.must(QueryBuilders.termQuery("make", query.getMake()));
            }
            if (StringUtils.isNotBlank(query.getColor())) {
                boolQueryBuilder.must(QueryBuilders.termQuery("color", query.getColor()));
            }

            searchSourceBuilder.query(boolQueryBuilder);
        }

        searchSourceBuilder.sort("id", SortOrder.ASC);
        return super.page(searchSourceBuilder, pageModel);
    }

    @Override
    public Object aggs1() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_aggregation_test_drive.html
        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("popular_colors").field("color.keyword");
        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        // 范围限定的聚合
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_scoping_aggregations.html
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.matchQuery("make", "ford"));
        searchSourceBuilder.query(boolQueryBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 按颜色聚合 + 均价
    @Override
    public Object aggs2() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_adding_a_metric_to_the_mix.html
        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("popular_colors").field("color.keyword");
        aggregationBuilder.subAggregation(AggregationBuilders.avg("avg_price").field("price"));
        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 按颜色聚合 + 均价 + 生产商
    @Override
    public Object aggs3() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_buckets_inside_buckets.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("popular_colors").field("color.keyword");

        AggregatorFactories.Builder builder = new AggregatorFactories.Builder();
        builder.addAggregator(AggregationBuilders.avg("avg_price").field("price"));
        builder.addAggregator(AggregationBuilders.terms("make").field("make.keyword"));
        aggregationBuilder.subAggregations(builder);

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 每个汽车生成商计算最低和最高的价格
    @Override
    public Object aggs4() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_one_final_modification.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("popular_colors").field("color.keyword");

        AggregatorFactories.Builder builder = new AggregatorFactories.Builder();
        builder.addAggregator(AggregationBuilders.avg("avg_price").field("price"));
        builder.addAggregator(AggregationBuilders.terms("make").field("make.keyword")
                .subAggregation(AggregationBuilders.max("max_price").field("price"))
                .subAggregation(AggregationBuilders.min("min_price").field("price")));
        aggregationBuilder.subAggregations(builder);

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 条形图
    @Override
    public Object aggs5() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_building_bar_charts.html#_building_bar_charts

        AggregationBuilder aggregationBuilder = AggregationBuilders.histogram("price").field("price")
                .interval(Double.parseDouble("20000"));

        AggregatorFactories.Builder builder = new AggregatorFactories.Builder();
        builder.addAggregator(AggregationBuilders.sum("revenue").field("price"));
        aggregationBuilder.subAggregations(builder);

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 条形图
    @Override
    public Object aggs6() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_building_bar_charts.html#_building_bar_charts

        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("makes").field("make.keyword").size(10);

        AggregatorFactories.Builder builder = new AggregatorFactories.Builder();
        builder.addAggregator(AggregationBuilders.extendedStats("stats").field("price"));
        aggregationBuilder.subAggregations(builder);

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 按时间统计
    @Override
    public Object aggs7() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_looking_at_time.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.dateHistogram("sales").field("sold")
                .dateHistogramInterval(DateHistogramInterval.MONTH).format("yyyy-MM-dd")
                // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_returning_empty_buckets.html
                .minDocCount(0L).extendedBounds(new ExtendedBounds("2014-01-01", "2014-12-31"));

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 按季度展示所有汽车品牌总销售额。同时按季度、按每个汽车品牌计算销售总额
    @Override
    public Object aggs8() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_extended_example.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.dateHistogram("sales").field("sold")
                .dateHistogramInterval(DateHistogramInterval.QUARTER).format("yyyy-MM-dd").minDocCount(0L)
                .extendedBounds(new ExtendedBounds("2014-01-01", "2014-12-31"));

        AggregatorFactories.Builder builder = new AggregatorFactories.Builder();
        builder.addAggregator(AggregationBuilders.terms("per_make_sum").field("make.keyword")
                .subAggregation(AggregationBuilders.sum("sum_price").field("price")));
        builder.addAggregator(AggregationBuilders.sum("total_sum").field("price"));
        aggregationBuilder.subAggregations(builder);

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 全局桶
    @Override
    public Object aggs9() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_scoping_aggregations.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.avg("single_avg_price").field("price");
        AggregationBuilder aggregationBuilder2 = AggregationBuilders.global("all")
                .subAggregation(AggregationBuilders.avg("avg_price").field("price"));

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);

        searchSourceBuilder.aggregation(aggregationBuilder);
        searchSourceBuilder.aggregation(aggregationBuilder2);

        // 查询
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.matchQuery("make", "ford"));
        searchSourceBuilder.query(boolQueryBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 过滤
    @Override
    public Object aggs10() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_filtering_queries.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.avg("single_avg_price").field("price");

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);

        // 查询
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.filter(QueryBuilders.rangeQuery("price").gte(10000));

        ConstantScoreQueryBuilder constantScoreQueryBuilder = QueryBuilders.constantScoreQuery(boolQueryBuilder);
        searchSourceBuilder.query(constantScoreQueryBuilder);

        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 后过滤器
    @Override
    public Object aggs11() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_post_filter.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("all_colors").field("color.keyword");

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();// .size(0);

        // 查询
        searchSourceBuilder.query(QueryBuilders.matchQuery("make.keyword", "ford"));

        searchSourceBuilder.postFilter(QueryBuilders.termQuery("color.keyword", "green"));

        searchSourceBuilder.aggregation(aggregationBuilder);
        LOG.info(searchSourceBuilder.toString());

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 桶排序
    @Override
    public Object aggs12() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_intrinsic_sorts.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("colors").field("color.keyword")
                .order(BucketOrder.aggregation("_count", true));

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 多桶排序 » 按度量排序
    @Override
    public Object aggs13() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_sorting_by_a_metric.html

        // 例子1
        /*AggregationBuilder aggregationBuilder = AggregationBuilders.terms("colors").field("color.keyword")
                .order(BucketOrder.aggregation("avg_price", true));
        aggregationBuilder.subAggregation(AggregationBuilders.avg("avg_price").field("price"));*/

        // 例子2
        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("colors").field("color.keyword")
                .order(BucketOrder.aggregation("stats.variance", true));
        aggregationBuilder.subAggregation(AggregationBuilders.extendedStats("stats").field("price"));

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 多桶排序 » 基于“深度”度量排序
    @Override
    public Object aggs14() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/_sorting_based_on_deep_metrics.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.histogram("colors").field("price").interval(20000D)
                .order(BucketOrder.aggregation("red_green_cars>stats.variance", true));
        aggregationBuilder.subAggregation(
                AggregationBuilders.filter("red_green_cars", QueryBuilders.termsQuery("color.keyword", "red", "green"))
                        .subAggregation(AggregationBuilders.extendedStats("stats").field("price")));

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }

    // 统计去重后的数量
    @Override
    public Object aggs15() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/cardinality.html

        AggregationBuilder aggregationBuilder = AggregationBuilders.cardinality("distinct_colors").field("color.hash")
                .precisionThreshold(100);// 配置精度 precision_threshold 接受 0–40,000 之间的数字，更大的值还是会被当作
                                         // 40,000 来处理

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);
        searchSourceBuilder.aggregation(aggregationBuilder);

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }
}
