package com.xuelang.es.service.impl;

import java.io.IOException;

import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.percentiles.PercentileRanksAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xuelang.es.base.service.BaseEsService;
import com.xuelang.es.model.Website;
import com.xuelang.es.service.WebsiteEsService;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Search;

@Service
public class WebsiteEsServiceImpl extends BaseEsService<Website> implements WebsiteEsService {
    private static final Logger LOG = LoggerFactory.getLogger(WebsiteEsServiceImpl.class);

    private static final String INDEX = "website";
    private static final String TYPE = "logs";

    @Autowired
    private JestClient jestClient;

    public WebsiteEsServiceImpl() {
        super(INDEX, TYPE);
    }

    @Override
    public Object agg1() {
        // https://www.elastic.co/guide/cn/elasticsearch/guide/current/percentiles.html

        // size 0 不返还具体记录
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0);

        // 例子1===========begin
        /*AggregationBuilder aggregationBuilder = AggregationBuilders.percentiles("load_times").field("latency");
        AggregationBuilder aggregationBuilder2 = AggregationBuilders.avg("avg_load_time").field("latency");
        
        searchSourceBuilder.aggregation(aggregationBuilder);
        searchSourceBuilder.aggregation(aggregationBuilder2);*/
        // 例子1===========end

        // 例子2===========begin
        /*AggregationBuilder aggregationBuilder = AggregationBuilders.terms("zones").field("zone");
        
        AggregatorFactories.Builder builder = new AggregatorFactories.Builder();
        builder.addAggregator(
                AggregationBuilders.percentiles("load_times").field("latency").percentiles(50d, 95.0d, 99.0d));
        builder.addAggregator(AggregationBuilders.avg("load_avg").field("latency"));
        
        aggregationBuilder.subAggregations(builder);
        
        searchSourceBuilder.aggregation(aggregationBuilder);*/
        // 例子2===========end

        // 例子3===========begin
        AggregationBuilder aggregationBuilder = AggregationBuilders.terms("zones").field("zone");

        double[] values = { 210, 800 };
        PercentileRanksAggregationBuilder percentileRanksAggregationBuilder = AggregationBuilders
                .percentileRanks("load_times", values).field("latency");

        aggregationBuilder.subAggregation(percentileRanksAggregationBuilder);

        searchSourceBuilder.aggregation(aggregationBuilder);
        // 例子3===========end

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(INDEX).addType(TYPE).build();

        try {
            JestResult result = jestClient.execute(search);
            return result.getJsonString();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }

        return null;
    }
}
