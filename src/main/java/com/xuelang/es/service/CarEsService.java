package com.xuelang.es.service;

import java.util.List;

import com.xuelang.es.base.dto.PageDto;
import com.xuelang.es.base.dto.PageModel;
import com.xuelang.es.base.service.IBaseEsService;
import com.xuelang.es.model.Car;

public interface CarEsService extends IBaseEsService<Car> {
    List<Car> list(Car query);

    PageDto<Car> page(Car query, PageModel pageModel);

    Object aggs1();

    Object aggs2();

    // 按颜色聚合 + 均价 + 生产商
    Object aggs3();

    Object aggs4();

    // 条形图
    Object aggs5();

    // 条形图
    Object aggs6();

    // 按时间统计
    Object aggs7();

    //
    Object aggs8();

    Object aggs9();

    // 过滤
    Object aggs10();

    // 后过滤器
    Object aggs11();

    // 桶排序
    Object aggs12();

    // 多桶排序 » 按度量排序
    Object aggs13();

    // 多桶排序 » 按度量排序
    Object aggs14();

    // 统计去重后的数量
    Object aggs15();
}
