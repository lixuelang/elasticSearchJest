package com.xuelang.es.service;

import com.xuelang.es.base.service.IBaseEsService;
import com.xuelang.es.model.Website;

public interface WebsiteEsService extends IBaseEsService<Website> {
    Object agg1();

}
