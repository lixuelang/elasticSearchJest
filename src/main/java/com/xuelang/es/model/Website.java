package com.xuelang.es.model;

import java.io.Serializable;

import io.searchbox.annotations.JestId;

public class Website implements Serializable {
    private static final long serialVersionUID = -863658353551774166L;

    @JestId
    private Long id;

    private Long latency;

    private String zone;

    private Long timestamp;

    public Website(Long id, Long latency, String zone, Long timestamp) {
        this.id = id;
        this.latency = latency;
        this.zone = zone;
        this.timestamp = timestamp;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLatency() {
        return latency;
    }

    public void setLatency(Long latency) {
        this.latency = latency;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
