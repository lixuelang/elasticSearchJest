package com.xuelang.es.model;

import java.io.Serializable;
import java.math.BigDecimal;

import io.searchbox.annotations.JestId;

public class Car implements Serializable {
    private static final long serialVersionUID = -863638353551774166L;

    // 不能放到父类中，放到父类中不识别 @JestId
    @JestId
    private Long id;

    private BigDecimal price;

    private String color;

    private String make;

    private Long sold;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Long getSold() {
        return sold;
    }

    public void setSold(Long sold) {
        this.sold = sold;
    }
}
