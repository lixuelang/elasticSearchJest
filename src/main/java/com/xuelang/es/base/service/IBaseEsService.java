package com.xuelang.es.base.service;

import java.util.List;

import org.elasticsearch.search.builder.SearchSourceBuilder;

import com.xuelang.es.base.dto.PageDto;
import com.xuelang.es.base.dto.PageModel;

import io.searchbox.core.BulkResult;

public interface IBaseEsService<T> {
    Boolean add(Object baseEsModel);

    BulkResult addList(List<T> list);

    T getById(long id);

    Boolean deleteById(long id);

    // 可局部更新
    Boolean update(long id, Object baseEsModel);

    List<T> search(SearchSourceBuilder searchSourceBuilder);

    PageDto<T> page(SearchSourceBuilder searchSourceBuilder, PageModel pageModel);
}
