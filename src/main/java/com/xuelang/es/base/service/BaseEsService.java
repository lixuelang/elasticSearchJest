package com.xuelang.es.base.service;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.xuelang.es.base.dto.PageDto;
import com.xuelang.es.base.dto.PageModel;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Bulk;
import io.searchbox.core.BulkResult;
import io.searchbox.core.Delete;
import io.searchbox.core.DocumentResult;
import io.searchbox.core.Get;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.Update;

public abstract class BaseEsService<T> implements IBaseEsService<T> {
    private static final Logger LOG = LoggerFactory.getLogger(BaseEsService.class);

    private String index;

    private String type;

    @Autowired
    private JestClient jestClient;

    public BaseEsService(String index, String type) {
        this.type = type;
        this.index = index;
    }

    public Boolean add(Object baseEsModel) {
        try {
            DocumentResult result = jestClient
                    .execute(new Index.Builder(baseEsModel).index(index).type(type).refresh(true).build());
            return result.isSucceeded();
        } catch (Exception e) {
            throw new RuntimeException("insert exception", e);
        }
    }

    public BulkResult addList(List<T> list) {
        Bulk.Builder bulk = new Bulk.Builder();
        for (Object entity : list) {
            bulk.addAction(new Index.Builder(entity).index(index).type(type).build());
        }
        try {
            BulkResult result = jestClient.execute(bulk.build());
            LOG.info("ES 插入完成");
            return result;
        } catch (IOException e) {
            LOG.error(e.getMessage());
            throw new RuntimeException("insert exception", e);
        }
    }

    public T getById(long id) {
        try {
            DocumentResult result = jestClient.execute(new Get.Builder(index, String.valueOf(id)).type(type).build());
            T t = result.getSourceAsObject(getTClass());
            return t;
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
        return null;
    }

    public Boolean deleteById(long id) {
        try {
            DocumentResult result = jestClient
                    .execute(new Delete.Builder(String.valueOf(id)).index(index).type(type).build());
            return result.isSucceeded();
        } catch (Exception e) {
            throw new RuntimeException("delete exception", e);
        }
    }

    public Boolean update(long id, Object baseEsModel) {
        String script = "{" + "\"doc\" : " + JSON.toJSONString(baseEsModel) + "}" + "}";
        try {
            DocumentResult result = jestClient.execute(
                    new Update.Builder(script).index(index).type(type).id(String.valueOf(id)).refresh(true).build());
            return result.isSucceeded();
        } catch (Exception e) {
            throw new RuntimeException("update exception", e);
        }
    }

    public List<T> search(SearchSourceBuilder searchSourceBuilder) {
        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(index).addType(type).build();
        try {
            JestResult result = jestClient.execute(search);
            return result.getSourceAsObjectList(getTClass());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return null;
    }

    public PageDto<T> page(SearchSourceBuilder searchSourceBuilder, PageModel pageModel) {

        PageDto<T> pageDto = new PageDto<>();

        int from = (pageModel.getPageNo() - 1) * pageModel.getPageSize();
        int size = pageModel.getPageSize();
        searchSourceBuilder.from(from);
        searchSourceBuilder.size(size);
        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(index).addType(type).build();

        try {
            JestResult result = jestClient.execute(search);
            pageDto.setResult(result.getSourceAsObjectList(getTClass()));
            pageDto.setTotal(((SearchResult) result).getTotal());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
        return pageDto;
    }

    private Class<T> getTClass() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
}
